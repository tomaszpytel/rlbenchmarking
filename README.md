# RLBenchmarking #

RLBenchmarking is a simple application designed to benchmark three popular reinforcement learning algorithms: Sarsa, Expected Sarsa and Q - learning.


### Testing environment ###
External 2D maze environment compatible with OpenAiGym

Details:
[gym-maze](https://github.com/MattChanTK/gym-maze)

## Options ##

### Avaiable algorithm's ratings: ###
* Cumulative reward
* Mean timestep reward
* Reward per episode
* Mean episode execution time
* First successful episode

### Algorithms variants: ###
* Q - learning
* Sarsa
* Expected Sarsa
* Q(λ)
* Sarsa(λ)
* Expected Sarsa(λ)

### Editable hyperparameters: ###
* Learning rate
* Exploration rate
* Discount factor
* Decay factor
* Number of episodes
* Number of steps

## How to run ##
There is a Windows executable generated by PyInstaller :)  
**RLBenchmarking -> setup/rlbenchmarking.exe**  
Tested on Windows 10 Enterprise x64  
