class RunData:
    def __init__(self, agent_name):
        self.agent_name = agent_name
        self.cumulative_reward = self.init_plot_data()
        self.reward_per_episode = self.init_plot_data()
        self.reward_per_step = self.init_plot_data()
        self.episodes_execution_times = []
        self.first_successful_episode = 0


    def init_plot_data(self):
        x_axis = []
        y_axis = []
        return [x_axis, y_axis]


    def append_x_y(self, array, x, y):
        array[0].append(x)
        array[1].append(y)
