import numpy as np
from operator import add
from matplotlib.figure import Figure
from TDAlgorithm.Evaluators.DataType.Table import Table
from TDAlgorithm.Evaluators.RunData import RunData


class AgentEvaluator:
    def __init__(self, train_data):
        self.data = self.prepare_data(train_data)

    def prepare_data(self, train_data):
        data = []
        if len(train_data[0]) == 1:
            data = [agent_data[0] for agent_data in train_data]
        else:
            for agent_data in train_data:
                data.append(self.calculate_mean_values(agent_data))
        return data

    def calculate_mean_values(self, agent_runs_data):
        mean_data = RunData(agent_runs_data[0].agent_name)

        mean_reward_per_episode = [0] * len(agent_runs_data[0].reward_per_episode[0])
        mean_episode_execution_time = [0] * len(agent_runs_data[0].episodes_execution_times)
        average_first_successful_episode = 0

        for run_data in agent_runs_data:
            mean_reward_per_episode = list(map(add, mean_reward_per_episode, run_data.reward_per_episode[1]))
            mean_episode_execution_time = list(map(add, mean_episode_execution_time, run_data.episodes_execution_times))
            average_first_successful_episode += run_data.first_successful_episode

        mean_reward_per_episode = list(map(lambda x: x/len(agent_runs_data), mean_reward_per_episode))
        mean_episode_execution_time = list(map(lambda x: x/len(agent_runs_data), mean_episode_execution_time))
        average_first_successful_episode = average_first_successful_episode/len(agent_runs_data)

        mean_data.reward_per_episode = [agent_runs_data[0].reward_per_episode[0], mean_reward_per_episode]
        mean_data.episodes_execution_times = mean_episode_execution_time
        mean_data.first_successful_episode = average_first_successful_episode

        return mean_data

    def get_reward_per_episode(self):
        figure = Figure(figsize=(6, 5), dpi=100)
        plt = figure.add_subplot(1, 1, 1)
        for agent_data in self.data:
            plt.plot(agent_data.reward_per_episode[0], agent_data.reward_per_episode[1], label=agent_data.agent_name)
        plt.legend()
        figure.suptitle('Reward per episode', fontsize=13)
        plt.set_ylabel("Reward", fontsize=14)
        plt.set_xlabel("Number of episodes", fontsize=13)
        return figure

    def get_cumulative_reward_per_step(self):
        figure = Figure(figsize=(6, 5), dpi=100)
        plt = figure.add_subplot(1, 1, 1)

        if not self.data[0].reward_per_step[0]:
            return "Not available for multi runs"

        for agent_data in self.data:
            plt.plot(agent_data.reward_per_step[0], agent_data.reward_per_step[1], label=agent_data.agent_name)
        plt.legend()
        figure.suptitle('Cumulative reward per timestep', fontsize=14)
        plt.set_ylabel("Reward", fontsize=14)
        plt.set_xlabel("Number of steps", fontsize=13)
        return figure

    def get_mean_reward_per_step(self):
        results = Table("Mean reward per step")

        if not self.data[0].reward_per_step[0]:
            return "Not available for multi runs"

        for agent_data in self.data:
            mean = np.mean(agent_data.reward_per_step[1])
            results.table_data[agent_data.agent_name] = mean
        return results

    def get_mean_episode_execution_time(self):
        results = Table("Episode mean execution time")
        for agent_data in self.data:
            mean = np.mean(agent_data.episodes_execution_times)
            results.table_data[agent_data.agent_name] = mean
        return results

    def get_first_successful_episode(self):
        results = Table("First successful episode")
        for agent_data in self.data:
            results.table_data[agent_data.agent_name] = agent_data.first_successful_episode
        return results

    def generate_rating(self, rating):
        return self.switcher(rating)()

    def switcher(self, arg):
        switch = {
            "Cumulative reward": self.get_cumulative_reward_per_step,
            "Mean reward": self.get_mean_reward_per_step,
            "Reward per episode": self.get_reward_per_episode,
            "Episode mean execution time": self.get_mean_episode_execution_time,
            "First successful episode": self.get_first_successful_episode
        }
        return switch.get(arg)
