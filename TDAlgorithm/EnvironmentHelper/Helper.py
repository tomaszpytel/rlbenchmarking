import numpy as np
import gym_maze


def prepare_env(env, is_on_policy):
    actions = env.action_space.n
    states = tuple((env.observation_space.high + np.ones(env.observation_space.shape)).astype(int))
    Q = init_Q_maze(states, actions, is_on_policy)
    return actions, states, Q


def init_Q_maze(states, actions, is_on_policy):
    if is_on_policy:
        return np.ones(states + (actions,), dtype=float)
    else:
        return np.zeros(states + (actions,), dtype=float)


def make_step(env, action):
    _state, reward, done, info = env.step(action)
    _state = convert_to_tuple(_state)
    return _state, reward, done, info


def reset_env(env):
    base_state = env.reset()
    return convert_to_tuple(base_state)


def convert_to_tuple(state):
    result = []
    for i in range(len(state)):
        index = int(round(state[i]))
        result.append(index)
    return tuple(result)
