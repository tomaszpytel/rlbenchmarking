import gym
import gym_maze
from Const.constants import ENVS_CODES


def get_environment(env_name):
    if env_name == "maze 10x10":
        return gym.make(ENVS_CODES[0])

    if env_name == "maze 100x100":
        return gym.make(ENVS_CODES[1])

    if env_name == "maze 10x10 plus":
        return gym.make(ENVS_CODES[2])

    if env_name == "maze 20x20 plus":
        return gym.make(ENVS_CODES[3])

    if env_name == "maze 30x30 plus":
        return gym.make(ENVS_CODES[4])

    return
