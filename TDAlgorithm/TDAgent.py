from TDAlgorithm.Evaluators.RunData import RunData
import numpy as np


class TDAgent:
    def __init__(self, agent_name, env, alpha, epsilon, gamma, n_episodes, max_steps, decay_factor, enable_ET, update_progress_bar):
        self.env = env
        self.learning_rate = alpha
        self.exploration_rate = epsilon
        self.discount_factor = gamma
        self.n_episodes = n_episodes
        self.max_steps = max_steps
        self.decay_factor = decay_factor
        self.enable_ET = enable_ET
        self.min_alpha = 0.2
        self.min_epsilon = 0.001
        self.trace_decay = 0.8
        self.run_data = RunData(agent_name)
        self.update_progress_bar = update_progress_bar

    def epsilon_greedy(self, Q, state):
        if np.random.rand() > self.exploration_rate:
            action = int(np.argmax(Q[state]))
        else:
            action = self.env.action_space.sample()
        return action

    def update_parameters(self, episode):
        self.learning_rate = self.update_parameter(self.min_alpha, self.learning_rate, episode)
        self.exploration_rate = self.update_parameter(self.min_epsilon, self.exploration_rate, episode)

    def update_parameter(self, min_value, current, episode):
        return max(min_value, current * (1 / (1 + self.decay_factor * episode)))

    def init_ET(self, states, actions):
        return np.zeros(states + (actions,), dtype=float)

    def is_on_policy(self):
        pass

    def run(self):
        pass

