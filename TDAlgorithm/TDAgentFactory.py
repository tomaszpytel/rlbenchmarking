from TDAlgorithm.Algorithms.Qlearning import QLearning
from TDAlgorithm.Algorithms.Sarsa import Sarsa
from TDAlgorithm.Algorithms.ExpectedSarsa import ExpectedSarsa
from Parameters.Parameters import Params


def get_agent(agent, env, update_progress):
    alpha, epsilon, gamma, n_episodes, n_steps, decay_factor = load_parameters()

    if agent == "Q - Learning":
        return QLearning(agent, env, alpha, epsilon, gamma, n_episodes, n_steps, decay_factor, False, update_progress)

    if agent == "Sarsa":
        return Sarsa(agent, env, alpha, epsilon, gamma, n_episodes, n_steps, decay_factor, False, update_progress)

    if agent == "Expected Sarsa":
        return ExpectedSarsa(agent, env, alpha, epsilon, gamma, n_episodes, n_steps, decay_factor, False,
                             update_progress)

    if agent == "Q(λ)":
        return QLearning(agent, env, alpha, epsilon, gamma, n_episodes, n_steps, decay_factor, True, update_progress)

    if agent == "Sarsa(λ)":
        return Sarsa(agent, env, alpha, epsilon, gamma, n_episodes, n_steps, decay_factor, True, update_progress)

    if agent == "Expected Sarsa(λ)":
        return ExpectedSarsa(agent, env, alpha, epsilon, gamma, n_episodes, n_steps, decay_factor, True,
                             update_progress)

    return


def load_parameters():
    alpha = Params.get("alpha")
    epsilon = Params.get("epsilon")
    gamma = Params.get("gamma")
    n_episodes = Params.get("number of episodes")
    n_steps = Params.get("number of steps")
    decay_factor = Params.get("decay factor")
    return alpha, epsilon, gamma, n_episodes, n_steps, decay_factor
