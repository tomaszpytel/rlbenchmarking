from TDAlgorithm.TDAgent import TDAgent
from TDAlgorithm.EnvironmentHelper.Helper import prepare_env, reset_env, make_step
import numpy as np
import time


class ExpectedSarsa(TDAgent):

    def is_on_policy(self):
        return True

    def run(self):
        actions, states, Q = prepare_env(self.env, self.is_on_policy())
        cumulative_reward = 0
        cumulative_reward_per_step = 0
        step_counter = 0
        is_solved = False
        for episode in range(self.n_episodes):
            start_time = time.time()
            if self.enable_ET:
                E = self.init_ET(states, actions)
            print("new episode: ", episode)
            base_state = reset_env(self.env)
            reward_sum = 0
            action = self.epsilon_greedy(Q, base_state)
            for step in range(self.max_steps):
                step_counter += 1
                _state, reward, done, info = make_step(self.env, action)
                reward_sum += reward

                expected_value = self.get_expected_value(Q, _state, actions)
                delta = reward + self.discount_factor * expected_value - Q[base_state + (action,)]
                if self.enable_ET:
                    E[base_state + (action, )] += 1
                    maze_length = states[0]
                    for x in range(maze_length):
                        for y in range(maze_length):
                            for a in range(actions):
                                Q[(x, y) + (a,)] += self.learning_rate * delta * E[(x, y) + (a,)]
                                E[(x, y) + (a,)] *= self.trace_decay * self.discount_factor
                else:
                    Q[base_state + (action, )] += self.learning_rate * delta

                _action = self.epsilon_greedy(Q, _state)
                cumulative_reward_per_step += reward
                self.run_data.append_x_y(self.run_data.reward_per_step, step_counter, cumulative_reward_per_step)
                base_state = _state
                action = _action

                if done or step == self.max_steps - 1:
                    if done and is_solved is False:
                        self.run_data.first_successful_episode = episode
                        is_solved = True
                    self.run_data.episodes_execution_times.append(round(time.time() - start_time, 5))
                    self.update_progress_bar()
                    cumulative_reward += reward_sum
                    self.run_data.append_x_y(self.run_data.reward_per_episode, episode, reward_sum)
                    self.run_data.append_x_y(self.run_data.cumulative_reward, episode, cumulative_reward)

                    print("Episode %d finished after %f time steps with total reward = %f."
                          % (episode, step, reward_sum))
                    break

            if episode == self.n_episodes - 1:
                print("finished all episodes")
            self.update_parameters(episode)
        return self.run_data

    def get_expected_value(self, Q, state, actions_n):
        selected_actions = Q[state]
        selected_actions_sum = sum(selected_actions)
        max_q = np.amax(Q[state])
        max_q_n = len(selected_actions[selected_actions == max_q])

        greedy_expected = max_q * ((1 - self.exploration_rate) / max_q_n + self.exploration_rate / actions_n) * max_q_n
        non_greedy_expected = (selected_actions_sum - max_q * max_q_n) * (self.exploration_rate / actions_n)
        return greedy_expected + non_greedy_expected
