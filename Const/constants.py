ENVS = [
    "maze 10x10",
    "maze 100x100",
    "maze 10x10 plus",
    "maze 20x20 plus",
    "maze 30x30 plus"
]

ENVS_CODES = [
    "maze-random-10x10-v0",
    "maze-random-100x100-v0",
    "maze-random-10x10-plus-v0",
    "maze-random-20x20-plus-v0",
    "maze-random-30x30-plus-v0"
]

RATINGS = [
    "Cumulative reward",
    "Mean reward",
    "Reward per episode",
    "Episode mean execution time",
    "First successful episode"
]

AGENTS = [
    "Q - Learning",
    "Sarsa",
    "Expected Sarsa",
    "Q(λ)",
    "Sarsa(λ)",
    "Expected Sarsa(λ)"
]
