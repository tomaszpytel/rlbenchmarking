import tkinter as tk


class CheckButtons(tk.Frame):
    def __init__(self, parent, items_to_list, label_text):
        self.parent = parent
        self.items = items_to_list
        self.label_text = label_text
        super().__init__(self.parent)

        self.selected_items = self.init_selected()

        check_buttons_frame = tk.Frame(self.parent, relief=tk.RAISED, borderwidth=1)

        label = tk.Label(check_buttons_frame, text=self.label_text, pady=13)
        label.pack()

        for index, item in enumerate(self.items):
            tk.Checkbutton(
                check_buttons_frame,
                variable=self.selected_items[index],
                offvalue="",
                onvalue=item,
                text=item,
                padx=20
            ).pack(anchor="w")

        check_buttons_frame.pack(side=tk.RIGHT, padx=30, pady=20)

    def init_selected(self):
        selection_variables = []
        for num in range(len(self.items)):
            selection_variables.append(tk.StringVar())
        return selection_variables
