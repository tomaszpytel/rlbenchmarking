import os
import tkinter as tk
from matplotlib.figure import Figure
from tkinter.ttk import Treeview
from tkinter import filedialog
import datetime
from TDAlgorithm.Evaluators.AgentEvaluator import AgentEvaluator
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from TDAlgorithm.Evaluators.DataType.Table import Table


class ResultsWindow(tk.Frame):
    def __init__(self, parent, train_data, ratings):
        super().__init__(parent)
        self.root = parent
        self.train_data = train_data
        self.ratings = ratings

        self.current_rating = tk.IntVar()

        self.new_window = self.init_window()
        self.sidenav_frame, self.content_frame, self.navigation_frame = self.init_frames()
        self.save_message = tk.Label(self.navigation_frame)

        self.init_sidenav()
        self.init_navigation()

        self.plots_frames = []
        self.data_to_save = []
        self.generate_plots()
        self.set_default_plot()

    def init_window(self):
        new_window = tk.Toplevel(self.root)
        new_window.title("Results")
        new_window.geometry("800x600+400+200")
        new_window.grab_set()
        return new_window

    def init_frames(self):
        navigation_frame = tk.Frame(self.new_window, relief=tk.RAISED, borderwidth=1)
        navigation_frame.pack(side="bottom", fill="x")

        sidenav_frame = tk.Frame(self.new_window, bg="gray64", borderwidth=1, relief=tk.RAISED)
        sidenav_frame.pack(side=tk.LEFT, fill=tk.Y)

        content_frame = tk.Frame(self.new_window)
        content_frame.pack(fill=tk.BOTH)

        return sidenav_frame, content_frame, navigation_frame

    def init_navigation(self):
        close_btn = tk.Button(self.navigation_frame, command=self.on_close_click, text="Close")
        save_btn = tk.Button(self.navigation_frame, command=self.on_save_click, text="Save")
        close_btn.pack(side=tk.RIGHT, padx=15, pady=5)
        save_btn.pack(side=tk.RIGHT, padx=15, pady=5)

    def init_sidenav(self):
        self.current_rating.set(0)
        for index, rating in enumerate(self.ratings):
            rb = tk.Radiobutton(self.sidenav_frame, text=rating, indicatoron=False, variable=self.current_rating,
                                value=index, command=self.on_click)
            rb.pack(side=tk.TOP, padx=20, pady=8, fill=tk.BOTH)

    def set_default_plot(self):
        self.on_click()

    def on_click(self):
        for frame in self.plots_frames:
            frame.pack_forget()
        self.plots_frames[self.current_rating.get()].pack()

    def generate_plots(self):
        evaluator = AgentEvaluator(self.train_data)

        for rating in self.ratings:
            frame = tk.Frame(self.content_frame)
            figure = evaluator.generate_rating(rating)
            self.data_to_save.append(figure)
            if type(figure) == Figure:
                self.create_plot(figure, frame)
            elif type(figure) == Table:
                self.create_table(figure, frame)
            else:
                self.create_message_label(figure, frame)
            self.plots_frames.append(frame)

    def create_plot(self, figure, frame):
        canvas = FigureCanvasTkAgg(figure, frame)
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True, padx=20, pady=20)

    def create_table(self, figure, frame):
        table = Treeview(frame)
        table["columns"] = ("first", "second")
        table["show"] = 'headings'
        table.heading("first", text="Agent name", anchor=tk.W)
        table.heading("second", text=figure.title, anchor=tk.W)
        for index, key in enumerate(figure.table_data):
            table.insert("", 0, values=(key, figure.table_data.get(key)))
        table.pack(pady=20, padx=20)

    def create_message_label(self, figure, frame):
        tk.Label(frame, text=figure).pack()

    def on_close_click(self):
        self.new_window.destroy()

    def on_save_click(self):
        dir = filedialog.askdirectory(initialdir="/", title="Select directory").replace('\\', '/')
        current_time = datetime.datetime.now().strftime("%m/%d/%Y_%H:%M:%S")
        timestamp = datetime.datetime.now().strftime("%d_%m_%y-%H-%M-%S")
        dir = f"{dir}/{timestamp}"
        os.makedirs(dir)
        log = open(f"{dir}/results.txt", "w+")
        log.write("time: " + current_time + '\n')
        try:
            for figure in self.data_to_save:
                if type(figure) == Figure:
                    title = figure._suptitle.get_text()
                    directory = f"{dir}/{title}"
                    figure.savefig(directory)
                elif type(figure) == Table:
                    log.write(figure.title + '\n')
                    for agent in figure.table_data.keys():
                        log.write(f"{agent}: {figure.table_data.get(agent)}\n")
                else:
                    print("Error")
            self.on_save_success()
        except:
            self.on_save_failure()

        self.save_message.pack(side=tk.RIGHT)

        log.close()

    def on_save_success(self):
        self.save_message.config(text='Data saved!', fg="green", font="bold")
        self.save_message.pack(side=tk.BOTTOM)

    def on_save_failure(self):
        self.save_message.config(text='Unable to save data!', fg="red", font="bold")
        self.save_message.pack(side=tk.BOTTOM)
