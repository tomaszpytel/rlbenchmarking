import tkinter as tk

from GUI.Navigation import Navigation
from GUI.Dropdown import Dropdown
from GUI.CheckButtons import CheckButtons
from GUI.CustomProgressBar import CustomProgressBar
from GUI.ResultsWindow import ResultsWindow
from GUI.ParametersTextBox import ParametersTextBox
from GUI.SetParametersWindow import SetParametersWindow
from GUI.ErrorWindow import ErrorWindow
from TDAlgorithm.TDAgentFactory import get_agent
from TDAlgorithm.EnvironmentHelper.EnvironmentFactory import get_environment

from Const.constants import AGENTS
from Const.constants import RATINGS
from Const.constants import ENVS


class Application(tk.Frame):
    def __init__(self, parent):
        super().__init__(parent)

        self.top_frame = tk.Frame(self)
        self.middle_frame = tk.Frame(self)
        self.bottom_frame = tk.Frame(self)

        self.dropdown_frame = tk.Frame(self.top_frame)
        self.env_dropdown = Dropdown(self.dropdown_frame, ENVS, "Select environment")
        self.runs_num_dropdown = Dropdown(self.dropdown_frame, [1, 10, 20, 50, 100], "Select number of runs per agent")
        self.agent_selection = CheckButtons(self.top_frame, AGENTS, "Select agents")
        self.rating_selection = CheckButtons(self.middle_frame, RATINGS, "Select ratings")

        self.parameters_text_box = ParametersTextBox(self.middle_frame)
        self.navigation = Navigation(self)

        self.root = parent

        self.load_widgets()

    def load_widgets(self):
        self.top_frame.pack(fill=tk.X)
        self.middle_frame.pack(fill=tk.X)
        self.bottom_frame.pack(fill=tk.X)

        self.env_dropdown.pack(side=tk.LEFT)
        self.runs_num_dropdown.pack(side=tk.LEFT)
        self.dropdown_frame.pack()
        self.agent_selection.pack()
        self.rating_selection.pack()
        self.parameters_text_box.pack()
        self.navigation.pack()

        self.pack(fill=tk.BOTH, expand=True)

    def on_exit_click(self):
        self.root.destroy()

    def on_set_parameters_click(self):
        SetParametersWindow(self)

    def on_train_click(self):
        agents = list(map(lambda y: y.get(),
                          list(filter(lambda x: (x.get() != ""),
                                      self.agent_selection.selected_items))))

        ratings = list(map(lambda y: y.get(),
                           list(filter(lambda x: (x.get() != ""),
                                       self.rating_selection.selected_items))))
        if not agents or not ratings:
            ErrorWindow(self, "You must choose agents and evaluation methods!")
            return

        env = get_environment(self.env_dropdown.selected_item.get())
        runs_num = int(self.runs_num_dropdown.selected_item.get())

        progress_bar = CustomProgressBar(self.bottom_frame, orient='horizontal', mode='indeterminate', length=200)
        progress_bar.pack()

        train_data = []
        for agent in agents:
            agent_data = []
            for i in range(runs_num):
                td_agent = get_agent(agent, env, progress_bar.step_with_update)
                agent_data.append(td_agent.run())
            train_data.append(agent_data)


        progress_bar.pack_forget()
        ResultsWindow(self, train_data, ratings)
