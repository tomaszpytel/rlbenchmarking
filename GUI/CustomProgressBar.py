from tkinter.ttk import Progressbar


class CustomProgressBar(Progressbar):
    def __init__(self, parent, **kwargs):
        Progressbar.__init__(self, parent, **kwargs)
        self.root = parent

    def step_with_update(self):
        self.step(5)
        self.root.update()
