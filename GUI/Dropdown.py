import tkinter as tk


class Dropdown(tk.Frame):
    def __init__(self, parent, items_to_list, label_text):
        self.parent = parent
        self.items_to_list = items_to_list
        self.label_text = label_text

        super().__init__(self.parent)

        self.selected_item = tk.StringVar()
        self.selected_item.set(self.items_to_list[0])

        dropdown_frame = tk.Frame(self.parent)

        label = tk.Label(dropdown_frame, text=self.label_text)
        drop = tk.OptionMenu(dropdown_frame, self.selected_item, *self.items_to_list)

        label.pack(side=tk.TOP, padx=8, anchor="nw")
        drop.pack(side=tk.TOP, padx=8, pady=8)

        dropdown_frame.pack(padx=30, pady=20)


        # self.selected_env = tk.StringVar()
        # self.selected_env.set(ENVS[0])
        #
        # dropdown_frame = tk.Frame(self.parent)
        #
        # label = tk.Label(dropdown_frame, text="Select environment")
        # drop = tk.OptionMenu(dropdown_frame, self.selected_env, *ENVS)
        #
        # label.pack(side=tk.TOP, padx=8, anchor="nw")
        # drop.pack(side=tk.TOP, padx=8, pady=8)
        #
        # dropdown_frame.pack(side=tk.LEFT, anchor="nw", padx=30, pady=20)
