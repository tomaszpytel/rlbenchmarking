import tkinter as tk
from Parameters.Parameters import Params


class SetParametersWindow(tk.Frame):
    def __init__(self, parent):
        super().__init__(parent)
        self.root = parent

        self.input_data = []
        self.new_window = self.init_window()

        self.set_params_frame = tk.Frame(self.new_window)
        self.btn_frame = tk.Frame(self.new_window)
        self.error_message = tk.Label(self.new_window)

        self.init_widgets()

    def init_window(self):
        new_window = tk.Toplevel(self.root)
        new_window.geometry("300x350+300+300")
        new_window.title("Set parameters")
        new_window.grab_set()
        return new_window

    def init_widgets(self):
        label_frame = tk.Frame(self.set_params_frame)
        entry_frame = tk.Frame(self.set_params_frame)

        cancel_btn = tk.Button(self.btn_frame, command=self.on_cancel_click, text="Cancel")
        save_btn = tk.Button(self.btn_frame, command=self.on_save_click, text="Save")

        cancel_btn.pack(side=tk.RIGHT, padx=10, pady=5)
        save_btn.pack(side=tk.RIGHT, padx=10)

        self.create_entries(label_frame, entry_frame)

        self.btn_frame.pack(side="bottom", fill="x")

        label_frame.pack(side=tk.LEFT)
        entry_frame.pack(side=tk.RIGHT)
        self.set_params_frame.pack(side="top", fill="both")

    def create_entries(self, label_frame, entry_frame):
        for param_key in Params:
            input_holder = tk.StringVar()
            self.input_data.append(input_holder)
            input_holder.set(Params.get(param_key))

            entry = tk.Entry(entry_frame, textvariable=input_holder)
            label = tk.Label(label_frame, text=param_key+":")

            label_frame.pack(side=tk.LEFT)
            entry_frame.pack(side=tk.RIGHT)

            entry.pack(padx=10, pady=11)
            label.pack(padx=10, pady=10)

    def validate(self):
        self.error_message.pack_forget()
        for data in self.input_data:
            try:
                float(data.get())
            except ValueError:
                self.error_message.config(text="Invalid data, try again", fg="red")
                self.error_message.pack(side=tk.BOTTOM, pady=10)
                return False
        return True

    def on_save_click(self):
        if self.validate():
            for index, param_key in enumerate(Params):
                if param_key == "number of episodes" or param_key == "number of steps":
                    Params[param_key] = int(self.input_data[index].get())
                else:
                    Params[param_key] = float(self.input_data[index].get())
            self.root.parameters_text_box.update_text()
            self.new_window.destroy()
        else:
            return

    def on_cancel_click(self):
        self.new_window.destroy()
