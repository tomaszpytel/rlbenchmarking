import tkinter as tk


class Navigation(tk.Frame):
    def __init__(self, parent):
        self.parent = parent
        super().__init__(self.parent)

        self.btn_frame = tk.Frame(self.parent, relief=tk.RAISED, borderwidth=1)
        exit_btn = tk.Button(self.btn_frame, command=self.parent.on_exit_click, text="Close")
        train_btn = tk.Button(self.btn_frame, command=self.parent.on_train_click, text="Train")
        set_parameters_button = tk.Button(self.btn_frame, command=self.parent.on_set_parameters_click, text="Set parameters")

        exit_btn.pack(side=tk.RIGHT, padx=10, pady=5)
        train_btn.pack(side=tk.RIGHT, padx=10)
        set_parameters_button.pack(side=tk.LEFT, padx=10)

        self.btn_frame.pack(side="bottom", fill="x")
