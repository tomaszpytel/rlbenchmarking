import tkinter as tk


class ErrorWindow(tk.Frame):
    def __init__(self, parent, error_message):
        super().__init__(parent)
        self.root = parent
        self.error_message = error_message

        self.error_window = self.init_window()
        self.display_error_message()
        self.init_navigation()

    def init_window(self):
        new_window = tk.Toplevel(self.root)
        new_window.title("Error")
        new_window.geometry("350x200+500+300")
        new_window.grab_set()

        return new_window

    def init_navigation(self):
        navigation_frame = tk.Frame(self.error_window, relief=tk.RAISED, borderwidth=1)
        ok_btn = tk.Button(navigation_frame, command=self.on_close_click, text="Close")
        ok_btn.pack(side=tk.RIGHT, padx=10, pady=5)
        navigation_frame.pack(side="bottom", fill="x")

    def display_error_message(self):
        message_frame = tk.Frame(self.error_window)
        message_container = tk.Label(message_frame, text=self.error_message, fg="red", font=("", 11))
        message_container.pack(pady=20)
        message_frame.pack(pady=20)

    def on_close_click(self):
        self.error_window.destroy()
