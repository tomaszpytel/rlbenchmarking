import tkinter as tk
from Parameters.Parameters import Params


class ParametersTextBox(tk.Frame):
    def __init__(self, parent):
        self.parent = parent
        super().__init__(self.parent)

        self.parameters_ui_frame = tk.Frame(self.parent)
        self.parameters_values = tk.Label(self.parameters_ui_frame)

        self.init_widgets()

    def init_widgets(self):
        self.parameters_ui_frame.config(relief=tk.RAISED, borderwidth=1)
        self.parameters_values.config(text=self.set_text(), width=25, justify=tk.LEFT, font=12)
        label = tk.Label(self.parameters_ui_frame, text="Parameters")

        label.pack()
        self.parameters_values.pack(side=tk.LEFT)
        self.parameters_ui_frame.pack(side=tk.LEFT, padx=10, pady=20, anchor="w")

    def set_text(self):
        parameters = \
            f"Alpha: {Params.get('alpha')}\n" \
            f"Epsilon: {Params.get('epsilon')}\n" \
            f"Gamma: {Params.get('gamma')}\n" \
            f"Number of episodes: {Params.get('number of episodes')}\n" \
            f"Number of steps: {Params.get('number of steps')}\n" \
            f"Decay factor: {Params.get('decay factor')}"
        return parameters

    def update_text(self):
        self.parameters_values.config(text=self.set_text())
