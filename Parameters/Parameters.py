Params = {
    "alpha": 0.9,
    "epsilon": 0.1,
    "gamma": 0.99,
    "number of episodes": 300,
    "number of steps": 150,
    "decay factor": 0.0001
}
